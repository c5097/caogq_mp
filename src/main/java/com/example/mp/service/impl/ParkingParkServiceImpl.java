package com.example.mp.service.impl;

import com.example.mp.entity.ParkingPark;
import com.example.mp.mapper.ParkingParkMapper;
import com.example.mp.service.IParkingParkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 停车场记录表 服务实现类
 * </p>
 *
 * @author caogq
 * @since 2022-01-04
 */
@Service
public class ParkingParkServiceImpl extends ServiceImpl<ParkingParkMapper, ParkingPark> implements IParkingParkService {

}
