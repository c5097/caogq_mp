package com.example.mp.service;

import com.example.mp.entity.ParkingPark;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 停车场记录表 服务类
 * </p>
 *
 * @author caogq
 * @since 2022-01-04
 */
public interface IParkingParkService extends IService<ParkingPark> {

}
