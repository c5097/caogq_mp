package com.example.mp.mapper;

import com.example.mp.entity.ParkingPark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 停车场记录表 Mapper 接口
 * </p>
 *
 * @author caogq
 * @since 2022-01-04
 */
public interface ParkingParkMapper extends BaseMapper<ParkingPark> {

}
