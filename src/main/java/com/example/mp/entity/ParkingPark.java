package com.example.mp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 停车场记录表
 * </p>
 *
 * @author caogq
 * @since 2022-01-04
 */
@TableName("parking_park")
public class ParkingPark implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 是否删除，1是，0否
     */
    private Integer deleted;

    /**
     * 停车场名称
     */
    private String parkName;

    /**
     * 高德地图唯一标识
     */
    private String parkingPoiid;

    /**
     * 所属单位id
     */
    private Long constructorId;

    /**
     * 所属单位名称（冗余）
     */
    private String constructor;

    /**
     * 该停车场在所属单位的编码
     */
    private String parkcodeInConstructor;

    /**
     * 停车场地址
     */
    private String address;

    /**
     * 停车场类型，1为居民小区、2为商圈停车场（购物中心商业广场商场等）、3为路侧停车、4为公园景点（景点乐园公园老街古镇等）、5为商务楼宇（酒店写字楼商务楼园区等）、6为其他、7为交通枢纽（机场火车站汽车站码头港口等）、8为市政设施（体育场博物图书馆医院学校等）
     */
    private Integer type;

    /**
     * 停车场客服电话
     */
    private String parkingMobile;

    /**
     * 支付方式。枚举支持：	1：表示支付宝在线缴费。	2：表示支付宝代扣缴费。	3：表示当面付。	说明：如支持多种方式以 ',' 进行分隔。
     */
    private String payType;

    /**
     * 收费说明
     */
    private String feeDescription;

    /**
     * 业务归属appid
     */
    private String businessAppid;

    /**
     * 业务归属pid
     */
    private String businessPid;

    /**
     * 业务收款pid
     */
    private String businessPayeeId;

    /**
     * 支付宝的parking_id
     */
    private String alipayParkingId;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 用户支付未离场的超时时间(以分钟为单位)
     */
    private Integer timeout;

    /**
     * 停车场车位数
     */
    private Integer sumSpace;

    /**
     * 支付宝的商户授权token
     */
    private String appAuthToken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }
    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }
    public String getParkingPoiid() {
        return parkingPoiid;
    }

    public void setParkingPoiid(String parkingPoiid) {
        this.parkingPoiid = parkingPoiid;
    }
    public Long getConstructorId() {
        return constructorId;
    }

    public void setConstructorId(Long constructorId) {
        this.constructorId = constructorId;
    }
    public String getConstructor() {
        return constructor;
    }

    public void setConstructor(String constructor) {
        this.constructor = constructor;
    }
    public String getParkcodeInConstructor() {
        return parkcodeInConstructor;
    }

    public void setParkcodeInConstructor(String parkcodeInConstructor) {
        this.parkcodeInConstructor = parkcodeInConstructor;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getParkingMobile() {
        return parkingMobile;
    }

    public void setParkingMobile(String parkingMobile) {
        this.parkingMobile = parkingMobile;
    }
    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }
    public String getFeeDescription() {
        return feeDescription;
    }

    public void setFeeDescription(String feeDescription) {
        this.feeDescription = feeDescription;
    }
    public String getBusinessAppid() {
        return businessAppid;
    }

    public void setBusinessAppid(String businessAppid) {
        this.businessAppid = businessAppid;
    }
    public String getBusinessPid() {
        return businessPid;
    }

    public void setBusinessPid(String businessPid) {
        this.businessPid = businessPid;
    }
    public String getBusinessPayeeId() {
        return businessPayeeId;
    }

    public void setBusinessPayeeId(String businessPayeeId) {
        this.businessPayeeId = businessPayeeId;
    }
    public String getAlipayParkingId() {
        return alipayParkingId;
    }

    public void setAlipayParkingId(String alipayParkingId) {
        this.alipayParkingId = alipayParkingId;
    }
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
    public Integer getSumSpace() {
        return sumSpace;
    }

    public void setSumSpace(Integer sumSpace) {
        this.sumSpace = sumSpace;
    }
    public String getAppAuthToken() {
        return appAuthToken;
    }

    public void setAppAuthToken(String appAuthToken) {
        this.appAuthToken = appAuthToken;
    }

    @Override
    public String toString() {
        return "ParkingPark{" +
            "id=" + id +
            ", createDate=" + createDate +
            ", updateDate=" + updateDate +
            ", version=" + version +
            ", deleted=" + deleted +
            ", parkName=" + parkName +
            ", parkingPoiid=" + parkingPoiid +
            ", constructorId=" + constructorId +
            ", constructor=" + constructor +
            ", parkcodeInConstructor=" + parkcodeInConstructor +
            ", address=" + address +
            ", type=" + type +
            ", parkingMobile=" + parkingMobile +
            ", payType=" + payType +
            ", feeDescription=" + feeDescription +
            ", businessAppid=" + businessAppid +
            ", businessPid=" + businessPid +
            ", businessPayeeId=" + businessPayeeId +
            ", alipayParkingId=" + alipayParkingId +
            ", cityCode=" + cityCode +
            ", timeout=" + timeout +
            ", sumSpace=" + sumSpace +
            ", appAuthToken=" + appAuthToken +
        "}";
    }
}
