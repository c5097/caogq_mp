package com.example.mp;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MpApplicationTests {

	@Test
	public void contextLoads() {
		String projectPath = System.getProperty("user.dir");
		log.info("路径："+projectPath);
		FastAutoGenerator.create("jdbc:mysql://localhost:3306/xtpark?useUnicode=true&characterEncoding=UTF8&serverTimezone=Asia/Shanghai",
				"root", "123456")
				.globalConfig(builder -> {
					builder.author("caogq") // 设置作者
							.fileOverride() // 覆盖已生成文件
							.outputDir(projectPath + "/src/main/java"); // 指定输出目录
				}).packageConfig(builder -> {
					builder.parent("com.example.mp") // 设置父包名
							.moduleName("") // 设置父包模块名
							.pathInfo(Collections.singletonMap(OutputFile.mapperXml, "F:\\project_1231\\caogq_mp\\src\\main\\resources\\com\\example\\mp")); // 设置mapperXml生成路径
				}).strategyConfig(builder -> {
					builder.addInclude("parking_park") // 设置需要生成的表名
							.addTablePrefix(); // 设置过滤表前缀
				}).templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
				.execute();




		System.out.println("=============生成了");
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Spring is here!");
	}
}
